<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 08.09.16
 * Time: 11:04
 */

namespace XSLFoProcessor;


use XSLFoProcessor\Renderer\IRenderer;

class XSLFoProcessor
{
    const FO_NS = "http://www.w3.org/1999/XSL/Format";
    /**
     * @var IRenderer
     */
    private $renderer = null;

    private $pageMasters = array();

    public function setRenderer(IRenderer $renderer) {

    }

    private function clear() {
        $this->pageMasters = array();
    }


    public function render($foContent) {
        $this->clear();

        $foDoc = new \DOMDocument();
        $foDoc->loadXML($foContent);

        $foXPath = new \DOMXPath($foDoc);
        $foXPath->registerNamespace("fo", self::FO_NS);

        $root = $foXPath->query("/fo:root")->item(0);

        $layoutMasters = $foXPath->query("./fo:layout-master-set/fo:simple-page-master", $root);

        for ($i = 0; $i < $layoutMasters->length; $i++) {
            $layoutMaster = $layoutMasters->item($i);

            $name = $layoutMaster->attributes->getNamedItem("master-name")->nodeValue;
            //TODO ETC

            $this->pageMasters[$name] = new PageMaster();
        }


        $pageSequences = $foXPath->query("./fo:page-sequence", $root);

        for ($i = 0; $i < $pageSequences->length; $i++) {
            $pageSequence = $pageSequences->item($i);

            $masterRef = $pageSequence->attributes->getNamedItem("master-reference")->nodeValue;

            $this->renderer->setPageMaster($this->pageMasters[$masterRef]);

            //TODO Find "flows" (only xsl-region-body is supported now)
            $foXPath->query("./fo:flow");
        }

    }

    public function getResult() {

    }

    public function saveResult($filename) {

    }

}
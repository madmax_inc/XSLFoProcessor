<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 08.09.16
 * Time: 17:04
 */

namespace XSLFoProcessor\Renderer;


use XSLFoProcessor\PageMaster;

interface IRenderer
{

    public function setPageMaster(PageMaster $pageMaster);

}